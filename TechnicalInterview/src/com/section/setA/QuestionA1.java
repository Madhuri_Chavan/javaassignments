package com.section.setA;

public class QuestionA1 {

	public static void main(String[] args) {
		Integer OuterLoopCounter = 1;
		
		//upper triangle
		for( ; OuterLoopCounter <= 4 ; OuterLoopCounter++)
		{
			//Print leading spaces
			printLeadingSpace(OuterLoopCounter);
			printIntegerRow(OuterLoopCounter);
		}
		
		OuterLoopCounter = 3;
		
		//Lower triangle
		for( ; OuterLoopCounter > 0 ; OuterLoopCounter--)
		{
			//Print leading spaces
			printLeadingSpace(OuterLoopCounter);
			printIntegerRow(OuterLoopCounter);
		}
		//End 
	}
	
	public static void printLeadingSpace(Integer number)
	{
		Integer SpaceCounter = 0;
		for( SpaceCounter = 5 - number ; SpaceCounter > 0 ; SpaceCounter--)
		{ 
			System.out.print(' ');
		}
	}
	
	public static void printIntegerRow(Integer OuterLoopCounter)
	{
		Integer InnerLoopCounter = 0;
		for( InnerLoopCounter = OuterLoopCounter ; InnerLoopCounter > 0 ; InnerLoopCounter--)
		{
			//Print numbers
			System.out.print(OuterLoopCounter);
			System.out.print(' ');
		}
		System.out.println();
	}

}
